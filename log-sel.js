// utility function to return integer in the range [lower,upper)
function randInt( lower , upper ) {
	return Math.floor( Math.random() * (upper - lower) + lower );
}

// download a text file
function download( filename , text ) {
	var pom = document.createElement('a');
	pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	pom.setAttribute('download', filename);
	pom.click();
}

// global variables

// dimension of the client
var W = 0;
var H = 0;

// array of functions to load experiment sequences
var loadSequence = []; // elements to be defined by individually included sequence files

// participant ID
participantId = 0;

// current technique
var techniques = {
	subdiv_key : 0,
	subdiv_tap : 1,
	mouse      : 2,
	touch      : 3
};
var techniqueNames = [
	'subdiv_key' ,
	'subdiv_tap' ,
	'mouse'      ,
	'touch'
];
var currTechnique = -1;

// namespace for SVG
var svgNS = "http://www.w3.org/2000/svg";

// format of a trial
// tgt : a target object containing the dimension and location of the target square
// tech : a string representing the technique used --
//        SUBDIV_KEY : numpad-based subdivision,
//        SUBDIV_TAP : direct-touch-based subdivision,
//        MOUSE      : mouse pointing,
//        TOUCH      : direct tapping
function trial( tgt , client , tech ) {
	this.target = {
		x : tgt.x,
		y : tgt.y,
		w : tgt.w,
		h : tgt.h
	};
	this.client = {
		w : client.w,
		h : client.h
	};
	this.techniqueId = tech;
	this.actions = [];
}
// format of an action to be recorded in a trial response
function action( k , x , y , t ) {
	this.key = k;
	this.x = x;
	this.y = y;
	this.timestamp = t;
}

// references to the grid lines and tiles
var grid = { hori:[], vert:[] };
var tile = [];

// reference to the current trial
var currTrial;

var trialGen; // trial generator

// global trial log object
var trialLog = [];

function trialSource( src ) {
	if (typeof(src)==='undefined') {
		return {
			next : function() {
				// randomly generate a target location based on the current window dimension
				var s = randInt(H/200,H/100);
				s = 5;
				var t = {
					x : randInt( W/5 , W*4/5 ),
					y : randInt( H/5 , H*4/5 ),
					w : s,
					h : s
				};
				return new trial( t , { w:W , h:H } , currTechnique );
			},
			progress : function() {
				return '-/-';
			}
		};
	} else { // return a trial from an array of trials
		var currTrialIndex = 0;
		return {
			next : function() {
				if (currTrialIndex == src.length) {
					return undefined;
				}
				var t = src[currTrialIndex++];
				var tgt  = {
					x : W/8 + W/4*t.col - t.size/2 ,
					y : H/8 + H/4*t.row - t.size/2 ,
					w : t.size,
					h : t.size
				};
				t.target = tgt;
				t.client = {w:W,h:H};
				t.techniqueId = techniques[ t.technique ];
				return t;
			},
			progress : function() {
				return currTrialIndex + "/" + src.length;
			}
		};
	}
}

// configuration of log-sel technique
var log_sel_conf = {
	numSubdiv : 3 , // number of subdivisions
	keys : {
		// 3-by-3 keys
		 97 : [2,0] ,
		 98 : [2,1] ,
		 99 : [2,2] ,
		100 : [1,0] ,
		101 : [1,1] ,
		102 : [1,2] ,
		103 : [0,0] ,
		104 : [0,1] ,
		105 : [0,2]
		// 2-by-2 keys
		// 97 : [1,0] ,
		// 98 : [1,1] ,
		//100 : [0,0] ,
		//101 : [0,1]
	},
	backKey : 96
};

var keyCodeMap = {
	 97 : '1' ,
	 98 : '2' ,
	 99 : '3' ,
	100 : '4' ,
	101 : '5' ,
	102 : '6' ,
	103 : '7' ,
	104 : '8' ,
	105 : '9' ,
	 96 : '0'
};

// update the size of the svg element
function updateSize() {
	W = document.documentElement.clientWidth;
	H = document.documentElement.clientHeight;
	var v = document.getElementById("svg-canvas");
	v.setAttributeNS(null,"width",W+"px");
	v.setAttributeNS(null,"height",H+"px");
	return;
}
window.addEventListener("load",function() {
	onSizeChange();
	hideElem(document.getElementById("start-button"));
	hideElem(document.getElementById("tech-change"));
});
window.addEventListener("resize",onSizeChange);

// redraw visuals on size change
function onSizeChange() {
	updateSize();
	//updateGrid();
	//updateTile();
	//updateTarget();
	return;
}

// set up target and grid
function clearAll() {
	document.getElementById("zone").innerHTML = null; // this clears the zone and the target
	document.getElementById("grid").innerHTML = null; // this clears the grid lines
	grid = {hori:[],vert:[]};
	document.getElementById("tile").innerHTML = null; // this clears the tiles
	tile = [];
}
function updateGrid() {
	var sub_div_base = log_sel_conf.numSubdiv;
	var gridElem = document.getElementById("grid");
	var vSp = H/sub_div_base; // vertical spacing
	var hSp = W/sub_div_base; // horizontal spacing
	for ( var i=1 ; i<sub_div_base ; ++i ) {
		var hLine = grid.hori[i-1] || document.createElementNS(svgNS,"line");
		hLine.setAttributeNS(null,"x1","0");
		hLine.setAttributeNS(null,"x2",W);
		hLine.setAttributeNS(null,"y1",vSp*i);
		hLine.setAttributeNS(null,"y2",vSp*i);
		hLine.setAttributeNS(null,"stroke-width","3");
		hLine.setAttributeNS(null,"stroke","red");
		hLine.setAttributeNS(null,"stroke-opacity","0.5");
		if (typeof(grid.hori[i-1]) === 'undefined') {
			gridElem.appendChild(hLine);
			grid.hori[i-1] = hLine;
		}
		var vLine = grid.vert[i-1] || document.createElementNS(svgNS,"line");
		vLine.setAttributeNS(null,"x1",hSp*i);
		vLine.setAttributeNS(null,"x2",hSp*i);
		vLine.setAttributeNS(null,"y1","0");
		vLine.setAttributeNS(null,"y2",H);
		vLine.setAttributeNS(null,"stroke-width","3");
		vLine.setAttributeNS(null,"stroke","red");
		vLine.setAttributeNS(null,"stroke-opacity","0.5");
		if (typeof(grid.vert[i-1]) === 'undefined') {
			gridElem.appendChild(vLine);
			grid.vert[i-1] = vLine;
		}
	}
	return;
}
function hideElem(elem) {
	elem.setAttribute("style","display:none;");
	return;
}
function showElem(elem) {
	elem.setAttribute("style","display:initial;");
	return;
}
function updateTile() {
	var sub_div_base = log_sel_conf.numSubdiv;
	var tileElem = document.getElementById("tile");
	var vSp = H/sub_div_base; // vertical spacing
	var hSp = W/sub_div_base; // horizontal spacing
	for ( var i=0 ; i<sub_div_base ; ++i ) {
		var currRow = tile[i] || [];
		for ( var j=0 ; j<sub_div_base ; ++j ) {
			var currTile = currRow[j] || {
				rectElem : document.createElementNS(svgNS,"rect") ,
				callback : function(myRow,myCol) {
					return function(ev) {
						zoom( myRow , myCol );
						//alert("zoom("+myRow+","+myCol+")");
						return;
					}
				}(i,j),
				attachEventListener : function() {
					this.rectElem.addEventListener("click",this.callback,true);
				},
				detachEventListener : function() {
					this.rectElem.removeEventListener("click",this.callback,true);
				}
			};
			currTile.rectElem.setAttributeNS(null,"x",hSp*j);
			currTile.rectElem.setAttributeNS(null,"y",vSp*i);
			currTile.rectElem.setAttributeNS(null,"width",hSp);
			currTile.rectElem.setAttributeNS(null,"height",vSp);
			currTile.rectElem.setAttributeNS(null,"opacity",0); // completely transparent
			//currTile.setAttributeNS(null,"fill","green"); // for debug only
			if (typeof(currRow[j]) === 'undefined') {
				currRow[j] = currTile;
			}
		}
		if (typeof(tile[i]) === 'undefined') {
			tile[i] = currRow;
			for (var j=0; j<sub_div_base; ++j) {
				tileElem.appendChild(tile[i][j].rectElem);
			}
		}
	}
	return;
}
function updateTarget() {
	var r = document.getElementById("target") || document.createElementNS(svgNS,"rect");
	var t = currTrial.target;
	r.setAttributeNS(null,"x",t.x);
	r.setAttributeNS(null,"y",t.y);
	r.setAttributeNS(null,"width",t.w);
	r.setAttributeNS(null,"height",t.h);
	if (document.getElementById("target") == null) {
		document.getElementById("zone").appendChild(r);
		r.setAttributeNS(null,"id","target");
	}
	return;
}

// zoom into a subdivision
function zoom( row , col ) {
	var zn = document.getElementById("zone");
	// figure out the transformation parameters
	var scale = log_sel_conf.numSubdiv;
	var mat = [
		scale , 0 , 
		0 , scale , 
		-col*W, -row*H 
	];
	// apply the transformation
	var existingTransformations = zn.getAttribute("transform") || "";
	zn.setAttribute("transform","matrix("+mat[0]+","+mat[1]+","+mat[2]+","+mat[3]+","+mat[4]+","+mat[5]+") "+existingTransformations);
	return;
}

// go back one level of subdivision
function back() {
	var zn = document.getElementById("zone");
	var existingTransformations = parseTransformations( zn.getAttribute("transform") || "" );
	if (existingTransformations.length == 0) {
		return;
	} else {
		existingTransformations[0] = "";
		zn.setAttribute("transform",combineTransformations(existingTransformations));
		return;
	}
}

// parse multiple transformations in a single attribute
function parseTransformations( val ) {
	var stages = val.split(")");
	var cleanStages = [];
	if (val.trim().length == 0) {
		return cleanStages;
	}
	for (stageIdx in stages) {
		var stage = stages[stageIdx];
		if (stage.trim().length != 0) {
			cleanStages.push(stage.replace(/ /g,"").trim()+")");
		}
	}
	return cleanStages;
}

// combine multiple transformations into a single attribute value
function combineTransformations( transf ) {
	var combined = "";
	for (t in transf) {
		combined = combined.concat(" ",transf[t]);
	}
	return combined.trim();
}

// respond to keyup event for subdivision navigation
function subdiv_key_callback( ev ) {
	if (ev.keyCode == log_sel_conf.backKey) {
		currTrial.actions = currTrial.actions || []; // in case the presented trial does not have an `actions' property
		currTrial.actions.push( new action(keyCodeMap[ev.keyCode],ev.x,ev.y,ev.timeStamp) );
		console.log(ev);
		back();
	} else if (ev.keyCode in log_sel_conf.keys) {
		var pos = log_sel_conf.keys[ev.keyCode];
		console.log(log_sel_conf.keys[ev.keyCode]);
		currTrial.actions = currTrial.actions || []; // in case the presented trial does not have an `actions' property
		if (currTrial.actions.length == 0) { // still waiting for the back key to be pressed to start the trial
			return;
		}
		zoom(pos[0],pos[1]);
		currTrial.actions.push( new action(keyCodeMap[ev.keyCode],ev.x,ev.y,ev.timeStamp) );
		if (isCorrect()) {
			console.log("correct");
			trialLog.push(currTrial);
			prepareNextTrial();
		} else {
			console.log("wrong");
		}
	}
	return;
}
function mouse_callback( ev ) {
	currTrial.actions = currTrial.actions || []; // in case the presented trial does not have an `actions' property
	currTrial.actions.push( new action(keyCodeMap[ev.keyCode],ev.x,ev.y,ev.timeStamp) );
	console.log(ev);
	if (isCorrect(ev)) {
		console.log("correct");
		trialLog.push(currTrial);
		prepareNextTrial();
	} else {
		console.log("wrong");
	}
	return;
}

// control the visibility of config page
window.addEventListener("keyup",function(ev) {
	if (ev.keyCode == 192 /* keyCode for tilde */) {
		showElem( document.getElementById("config-dialog") );
	}
	return;
});

// set technique
function enableTechnique( tId ) {
	switch (tId) {
		case 0: // subdiv_key
			window.addEventListener("keyup",subdiv_key_callback);
			break;
		case 1: // subdiv_tap
			for (var i=0; i<log_sel_conf.numSubdiv; ++i) {
				for (var j=0; j<log_sel_conf.numSubdiv; ++j) {
					tile[i][j].attachEventListener();
				}
			}
			break;
		case 2: // mouse
			document.getElementById("svg-canvas").addEventListener("click",mouse_callback,true);
			break;
		case 3: // touch
			document.getElementById("svg-canvas").addEventListener("click",mouse_callback,true);
			break;
		default:
			break;
	}
	return;
}
function disableTechnique( tId ) {
	switch (tId) {
		case 0: // subdiv_key
			window.removeEventListener("keyup",subdiv_key_callback);
			break;
		case 1: // subdiv_tap
			for (var i=0; i<log_sel_conf.numSubdiv; ++i) {
				for (var j=0; j<log_sel_conf.numSubdiv; ++j) {
					tile[i][j].detachEventListener();
				}
			}
			break;
		case 2: // mouse
			document.getElementById("svg-canvas").removeEventListener("click",mouse_callback,true);
			break;
		case 3: // touch
			document.getElementById("svg-canvas").removeEventListener("click",mouse_callback,true);
			break;
		default:
			break;
	}
	return;
}
function setTechnique( tId ) {
	if (tId != currTechnique) {
		disableTechnique(currTechnique);
		enableTechnique(tId);
		notifyTechniqueChange(tId);
	}
	unregisterStartAction(currTechnique);
	registerStartAction(tId);
	currTechnique = tId;
	return;
}
function notifyTechniqueChange(techId) {
	document.getElementById("new-tech").innerHTML = techniqueNames[techId];
	showElem( document.getElementById("tech-change") );
	return;
}

function updateProgress() {
	document.getElementById("prog").innerHTML = trialGen.progress()+"";
	return;
}

function showStartButton() {
	updateProgress();
	showElem( document.getElementById("start-button") );
	return;
}

function compileResultAsCSV( res ) {
	var text = 'pid,technique,size,row,col,target_x,target_y,target_w,target_h,client_w,client_h,repetition,duration,num_stages\n';
	for (i in res) {
		var t = res[i];
		var a = res[i].actions;
		var dur = a[a.length-1].timestamp - a[0].timestamp;
		text += (
				participantId + ',' +
				t.technique + ',' +
				t.size + ',' +
				t.row + ',' +
				t.col + ',' +
				t.target.x + ',' +
				t.target.y + ',' +
				t.target.w + ',' +
				t.target.h + ',' +
				t.client.w + ',' +
				t.client.h + ',' +
				t.repetition + ',' +
				dur + ',' +
				a.length + '\n'
				);
	}
	return text;
}

function compileResultAsJSON( res ) {
	for (i in res) {
		res[i].pid = participantId;
		var a = res[i].actions;
		res[i].duration = a[a.length-1].timestamp - a[0].timestamp;
		res[i].numStages = a.length;
	}
	return JSON.stringify(res,null,'\t');
}

function previewResult( res ) {
	document.getElementById("csv-preview").value = compileResultAsCSV( res );
	document.getElementById("json-preview").value = compileResultAsJSON( res );
}

function prepareNextTrial() {
	currTrial = trialGen.next();
	hideElem( document.getElementById("config-dialog") );
	hideElem( document.getElementById("svg-canvas") );
	document.getElementById("zone").removeAttribute("transform");
	if (typeof(currTrial) === 'undefined') { // an experiment is finished
		// compile the results into textual CSV and JSON formats
		previewResult( trialLog );
		// offer a save button to download the results
		showElem( document.getElementById("save-dialog") );
		reset();
		return;
	}
	updateGrid();
	updateTile();
	updateTarget();
	setTechnique(currTrial.techniqueId);
	showStartButton();
	return;
}

function btn_apply_clicked() {
	var session_conf = getConfig();
	closeConfig();
	//console.log(techId);
	var newId = session_conf.pId.trim();
	participantId = (newId.length == 0) ? participantId : newId;
	switch (session_conf.sessionId) {
		case 0:
			setTechnique(session_conf.techId);
			trialGen = trialSource(); // a random trial source
			break;
		default:
			trialGen = trialSource( loadSequence[session_conf.sessionId]() ); // a trial source from file
			break;
	}
	prepareNextTrial();
	showStartButton();
	hideElem( document.getElementById("svg-canvas") );
	return;
}

// get configuration of the technique and session
function getConfig() {
	var tech = techniques[ document.getElementById("sel-technique").selectedOptions[0].value ];
	var session = document.getElementById("sel-session").selectedIndex; // 0 for practice, positive for experiment
	var p = document.getElementById("pid").value;
	return {techId:tech,sessionId:session,pId:p};
}

function closeConfig() {
	hideElem( document.getElementById('config-dialog') );
	return;
}

function btn_cancel_clicked() {
	closeConfig();
	return;
}

function btn_start_clicked() {
	hideElem( document.getElementById("start-button") );
	showElem( document.getElementById("svg-canvas") );
	unregisterStartAction(currTechnique); // start button can only be pressed once per trial
	return;
}

function getTransformedTarget() {
	var t = document.getElementById("target");
	var mat = t.getCTM();
	var tx = t.getAttribute("x");
	var ty = t.getAttribute("y");
	var tw = t.getAttribute("width");
	var th = t.getAttribute("height");
	return {
		x : tx*mat.a+ty*mat.c+mat.e ,
		y : tx*mat.b+ty*mat.d+mat.f ,
		w : tw*mat.a ,
		h : th*mat.d
	};
}

function isCorrect( cond ) {
	switch (currTechnique) {
		case 0: // subdiv_key
		case 1: // subdiv_tap
			// check whether the target has filled the entire screen
			var tt = getTransformedTarget();
			if (tt.x > 0 || (tt.x+tt.w-1) < W) {
				return false;
			}
			if (tt.y > 0 || (tt.y+tt.h-1) < H) {
				return false;
			}
			return true;
			break;
		case 2: // mouse
		case 3: // touch
			// cond is a mouse click or touch event, do hit test on the target
			var dx = cond.x - currTrial.target.x;
			var dy = cond.y - currTrial.target.y;
			if (dx < 0 || dx >= currTrial.target.w) {
				return false;
			}
			if (dy < 0 || dy >= currTrial.target.h) {
				return false;
			}
			return true;
			break;
		default:
			alert('Unknown technique!');
			break;
	}
	return false;
}

function backToStart( ev ) {
	if (ev.keyCode == log_sel_conf.backKey) {
		btn_start_clicked();
	}
	return;
}

function clickToStart(ev) {
	console.log(ev);
	currTrial.actions = currTrial.actions || [];
	currTrial.actions.push( new action(keyCodeMap[ev.keyCode],ev.x,ev.y,ev.timeStamp) );
	btn_start_clicked(ev);
	return;
}
	
function registerStartAction(tId) {
	switch (tId) {
		case 0: // subdiv_key
			window.addEventListener("keyup",backToStart);
			break;
		case 1: // subdiv_tap
			break;
		case 2: // mouse
			document.getElementById("btn-start").addEventListener("click",clickToStart);
			break;
		case 3: // touch
			break;
	}
	return;
}
function unregisterStartAction(tId) {
	switch (tId) {
		case 0: // subdiv_key
			window.removeEventListener("keyup",backToStart);
			break;
		case 1: // subdiv_tap
			break;
		case 2: // mouse
			document.getElementById("btn-start").removeEventListener("click",clickToStart);
			break;
		case 3: // touch
			break;
	}
	return;
}

// reset the program status
// alternatively, refresh the page
function reset() {
	trialLog = [];
	currTrial = undefined;
	currTechnique = -1;
	participantId = 0;
	for (var i=0; i<4; ++i) {
		unregisterStartAction(i);
		disableTechnique(i);
	}
}

